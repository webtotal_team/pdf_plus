<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

require_once ('../vendor/autoload.php');
require_once('../src/PDF_WriteTag.php');
require_once('../src/FPDF_PLUS.php');
require_once('../src/PDF_Invoice.php');

// Dummy Database
$lines = [
    ["Quantity and Description" => "1 Ali Baba And The Bongo Bandits - Script (Editable Word Doc) Download",
        "Prod.\nCode" => "ABAWD02",
        "Unit\nPrice" => "10.00",
        "Net\nAmount" => "10.00",
        "Disc\nRate" => "0.00",
        "Vat\nRate" => "20.00",
        "Vat" => "2"],
    ["Quantity and Description" => "1 Ali Baba And The Bongo Bandits - Backing Tracks Download",
        "Prod.\nCode" => "ABAD06",
        "Unit\nPrice" => "10.00",
        "Net\nAmount" => "10.00",
        "Disc\nRate" => "0.00",
        "Vat\nRate" => "20.00",
        "Vat" => "2"],
    ["Quantity and Description" => "1 Ali Baba And The Bongo Bandits - Vocal Tracks Download",
        "Prod.\nCode" => "ABAD09",
        "Unit\nPrice" => "10.00",
        "Net\nAmount" => "10.00",
        "Disc\nRate" => "0.00",
        "Vat\nRate" => "20.00",
        "Vat" => "2"],
    ["Quantity and Description" => "1 The Pied Piper - Script (Editable Word Doc) Download",
        "Prod.\nCode" => "PPJWD02",
        "Unit\nPrice" => "10.00",
        "Net\nAmount" => "10.00",
        "Disc\nRate" => "0.00",
        "Vat\nRate" => "20.00",
        "Vat" => "2"],
    ["Quantity and Description" => "1 The Pied Piper - Backing Tracks Download",
        "Prod.\nCode" => "PPJD06",
        "Unit\nPrice" => "10.00",
        "Net\nAmount" => "10.00",
        "Disc\nRate" => "0.00",
        "Vat\nRate" => "20.00",
        "Vat" => "2"],
    ["Quantity and Description" => "1 The Pied Piper - Vocal Tracks Download",
        "Prod.\nCode" => "PPJD09",
        "Unit\nPrice" => "10.00",
        "Net\nAmount" => "10.00",
        "Disc\nRate" => "0.00",
        "Vat\nRate" => "20.00",
        "Vat" => "2"]
];

$totals = [];
$totals['sub'] = '10.00';
$totals['delivery'] = '0.00';
$totals['vat'] = '2.00';
$totals['total'] = '12.50';

$config = [
    'unit' => 'mm',
    'size' => 'A4',
    'orientation' => 'P',
    'margin_left' => 20,
    'margin_right' => 15,
    'margin_top' => 48,
    'margin_bottom' => 25,
    'margin_header' => 0,
    'margin_footer' => 0
];
$pdf = new PDF_Invoice( 'P', 'mm', 'A4' );
$pdf->SetDisplayMode('fullpage');
$pdf->AddPage();
$pdf->SetMargins(0, 0,0);
$pdf->SetAutoPageBreak(false);

// Stylesheet
$pdf->SetStyle("p","arial","N",8,"0,0,0");
$pdf->SetStyle("pl","arial","N",10,"0,0,0");
$pdf->SetStyle("pxl","arial","B",14,"105,105,105");
$pdf->SetStyle("b","arial","B",8,"0,0,0");
$pdf->SetStyle("bl","arial","B",10,"0,0,0");
$pdf->SetStyle("bxl","arial","B",14,"105,105,105");
$pdf->SetStyle("red","arial","B",8,"255,0,0");
$pdf->SetStyle("redl","arial","B",10,"255,0,0");
$pdf->SetStyle("redxl","arial","B",14,"255,0,0");
$pdf->SetStyle("invoice","arial","B",12,"255,0,0");

$path = "logo_dark.png";
$pdf->addLogo($path);
$pdf->AddCompanyName("Webtotal.");
$pdf->AddAccountNumber("29 54 12 71");
$pdf->AddAccountSortcode("60-21-50");
$pdf->AddVatNumber("GB 669 9835 51");
$pdf->AddEmail("sarah@musicline-ltd.com");
$pdf->AddInvoiceDate("22/01/2020");

$path = "Stamp_Paid.png";
$pdf->addImage($path, 123, 240, 58.137777778,37.112222222, 0);

$pdf->addDeliveryAddress("Mark Morris",
    "123 Address line 1 \n" .
    "Address line 2 \n".
    "City \n" .
    "County \n" .
    "State \n" .
    "Post Code \n" .
    "Country");


$pdf->addCompanyAddress("Webtotal\n Company Address 1, Company Address 2, \nCity, County, \nState, Postcode \nTel: xxxxx xxx xxx \nFax: xxxxx xxx xxx \nEmail: me@domain.com \nWeb: www.webtotal.com");
$pdf->LeftColumn(['Mark Morris', '123 Address line 1', 'Address line 2', 'City', 'County', 'Post Code', 'United Kingdom']);

$pdf->RightColumn(
    ['Invoice Number:', 'Invoice Date:', 'Your Account Ref:', 'Invoice Number:', 'Your Order Ref./No.:'],
    ['167498', '22/01/2020', 'RH14QJ', '167498', 'W196508ACUR']
);

$cols = array("Quantity and Description" => 108, "Prod.\nCode" => 20, "Unit\nPrice" => 14, "Net\nAmount" => 14, "Disc\nRate" => 14, "Vat\nRate" => 14, "Vat" => 14);
$pdf->addCols($cols);

$cols = array("Quantity and Description" => "L", "Prod.\nCode" => "C", "Unit\nPrice" => "C", "Net\nAmount" => "C", "Disc\nRate" => "C", "Vat\nRate" => "C", "Vat" => "C");
$pdf->addLineFormat($cols);

$y = 132;
foreach($lines as $line) {
    $size = $pdf->addLine($y, $line);
    $y += $size + 2;
}

$pdf->AddFooter($totals);

$pdf->Output();
