<?php

// Mark Morris
// Version 1.1.15

class FPDF_PLUS extends PDF_WriteTag
{
    public string $footer_left_text = "";
    public string $footer_right_text = "";

    /**
     * @param string $path
     * @param int $x
     * @param int $y
     * @param int $w
     * @param int $h
     * @param float $border
     * @param array $color
     * @desctription: This will add an image to the page, if the image dont exist it will use a fill graphic.
     */
    function addImage($path, $x, $y, $w, $h, $border = 0.5, $color = [0,0,0]) {

        if ($border > 0) {
            $this->SetFillColor($color[0], $color[1], $color[2]);
            $this->Rect($x - $border, $y  - $border, $w  + $border * 2, $h  + $border * 2, 'F');
        }

        if (isset($path) && strlen($path) > 0) {
            $this->Image($path, $x, $y, $w, $h);
        } else {
            $this->SetFillColor($color[0], $color[1], $color[2]);
            $this->Rect($x, $y , $w , $h, 'F');
        }
    }

    /**
     * @param $text
     * @param $x
     * @param $y
     * @param $offsetX
     * @param $offsetY
     * @param $maxWidth
     * @param string $font
     * @param int $fontSize
     * @param array $color
     * @param string $align
     */
    function addTextMaxWidth($text, $x, $y, $offsetX, $offsetY, $maxWidth, $font = 'Arial', $fontSize = 10, $color = [0,0,0], $align = 'left') {

        // Get original text Width
        $this->SetFont($font, '', $fontSize);
        $textWidth = $this->GetStringWidth($text);

        if ($textWidth > $maxWidth) {
            $newString = $text;
            for ($loopS = 0; $loopS < strlen($text); $loopS++) {

                // Reduce String by 1 & Get the new Width of the string
                $newString = substr($newString, 0, -1);
                $textWidth = $this->GetStringWidth($newString);

                // If the width fits out column then GOOD!
                if ($textWidth < $maxWidth) {
                    $value = strrpos($newString, ' ');
                    $str1 = substr($text, 0, $value);
                    $str2 = trim(substr($text, $value));

                    $textWidth = $this->GetStringWidth($str1);
                    $this->addText($str1, $x - ($align==='center'?($textWidth / 2):0), $y, $font, $fontSize, $color);

                    $this->addTextMaxWidth($str2, $x, $y + $offsetY, $offsetX, $offsetY, $maxWidth, $font, $fontSize, $color, $align);
                    break;
                }
            }
        } else {
            $textWidth = $this->GetStringWidth($text);
            $this->addText($text, $x - ($align==='center'?($textWidth / 2):0), $y, $font, $fontSize, $color);
        }

    }

    /**
     * @param $text
     * @param $x
     * @param $y
     * @param $offsetX
     * @param $offsetY
     * @param $sW
     * @param $iW
     * @param string $font
     * @param int $fontSize
     * @param array $color
     * @param string $align
     */
    function addTextDynamicWidth($text, $x, $y, $offsetX, $offsetY, $sW, $iW, $font = 'Arial', $fontSize = 10, $color = [0,0,0], $align = 'left') {

        // Get original text Width
        $this->SetFont($font, '', $fontSize);
        $textWidth = $this->GetStringWidth($text);

        if ($textWidth > $sW) {
            $newString = $text;
            for ($loopS = 0; $loopS < strlen($text); $loopS++) {

                // Reduce String by 1 & Get the new Width of the string
                $newString = substr($newString, 0, -1);
                $textWidth = $this->GetStringWidth($newString);

                // If the width fits out column then GOOD!
                if ($textWidth < $sW) {
                    $value = strrpos($newString, ' ');
                    $str1 = substr($text, 0, $value);
                    $str2 = trim(substr($text, $value));

                    if (substr_count($str2, ' ', 0) < 2) {
                        $value = strrpos($str1, ' ');
                        $str1 = substr($text, 0, $value);
                        $str2 = trim(substr($text, $value));
                    }

                    $textWidth = $this->GetStringWidth($str1);
                    $this->addText($str1, $x - ($align==='center'?($textWidth / 2):0), $y, $font, $fontSize, $color);
                    $this->addTextDynamicWidth($str2, $x, $y + $offsetY, $offsetX, $offsetY, $sW + $iW, $iW, $font, $fontSize, $color, $align);
                    break;
                }
            }
        } else {
            $textWidth = $this->GetStringWidth($text);
            $this->addText($text, $x - ($align==='center'?($textWidth / 2):0), $y, $font, $fontSize, $color);
        }
    }

    /**
     * @param string $text
     * @param $x
     * @param $y
     * @param string $font
     * @param int $fontSize
     * @param array $color
     * @param string $align
     */
    function addText($text, $x, $y, $font = 'Arial', $fontSize = 10, $color = [0,0,0], $align = 'left') {
        $this->SetFont($font, '', $fontSize);
        $this->SetTextColor($color[0],$color[1], $color[2]);
        $textWidth = $this->GetStringWidth($text);
        $this->Text($x - ($align==='center'?($textWidth / 2):0), $y, $text);
    }

    /**
     * @param $text
     * @param $x
     * @param $y
     * @param $w
     * @param string $font
     * @param int $maxFontSize
     * @param array $color
     * @param string $align
     */
    function addFitText($text, $x, $y, $w, $maxFontSize = 50, $color = [0,0,0], $align = 'left') {
        $this->SetFontSize($maxFontSize);
        $this->SetTextColor($color[0],$color[1], $color[2]);
        $textWidth = $this->GetStringWidth($text);

        for ($fontSize = $maxFontSize; $textWidth > $w; $fontSize--) {
            $this->SetFontSize($fontSize);
            $textWidth = $this->GetStringWidth($text);
        }

        $this->Text($x - ($align==='center'?($textWidth / 2):0), $y, $text);
    }

    function Footer()
    {
        // Go to 1.5 cm from bottom
        $this->SetY(-15);
        // Select Arial italic 8
        $this->SetFont('Arial','I',8);
        // Print current and total page numbers
        $this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'C');

        if (strlen($this->footer_right_text)) {
            $this->Cell(0, 10, $this->footer_right_text, 0, 0, 'R');
        }

        $this->SetX(8);
        if (strlen($this->footer_left_text)) {
            $this->Cell(0, 10, $this->footer_left_text, 0, 0, 'L');
        }

    }

    function SetFooterRightText($footer_text) {
        $this->footer_right_text = $footer_text;
    }

    function SetFooterLeftText($footer_text) {
        $this->footer_left_text = $footer_text;
    }

}
