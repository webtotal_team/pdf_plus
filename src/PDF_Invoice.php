<?php

// Mark Morris
// Version 1.0.0

class PDF_Invoice extends FPDF_PLUS
{
    var $company_name = '';
    var $account_number = '';
    var $account_sortcode = '';
    var $vat_number = '';
    var $email = '';
    var $invoice_full_name = '';
    var $invoice_date = '';

    var $columns;
    var $format;
    var $angle=0;
    var $B=0;
    var $I=0;
    var $U=0;
    var $HREF='';
    var $ALIGN='';
    var $lineHeight = 5;
    var $textBuffer = '';

    function AddInvoiceDate($date) {
        $this->invoice_date = $date;
    }

    function AddInvoiceName($name) {
        $this->invoice_full_name = $name;
    }

    function AddCompanyName($name) {
        $this->company_name = $name;
    }

    function AddAccountNumber($no) {
        $this->account_number = $no;
    }

    function AddAccountSortcode($sortcode) {
        $this->account_sortcode = $sortcode;
    }

    function AddVatNumber($no) {
        $this->vat_number = $no;
    }

    function AddEmail($email) {
        $this->email = $email;
    }

    // Company
    function addDeliveryAddress( $nom, $adresse )
    {
        $x1 = 6;
        $y1 = 15;
        $this->SetXY( $x1, $y1 );
        $this->SetFont('Arial','B',25);
        $this->Text($x1, $y1, 'Invoice');

        $this->SetXY( $x1+34, $y1 -7.5 );
        $this->WriteTag(0, 5, '<invoice>Important: Please pass this invoice</invoice>');
        $this->SetXY( $x1+34, $y1 -3.5 );
        $this->WriteTag(0, 5, '<invoice>to your finance team</invoice>');

        $this->SetTextColor(0,0,0);

        $x1 = 12;
        $y1 = 25;
        $this->SetXY( $x1, $y1 );
        $this->SetFont('Arial','B',12);
        $length = $this->GetStringWidth( $nom );
        $this->Cell( $length, 2, $nom);
        $this->SetXY( $x1, $y1 + 4 );
        $this->SetFont('Arial','',10);
        $length = $this->GetStringWidth( $adresse );

        $this->MultiCell($length, 5, $adresse);
    }

    function addLogo($path) {
        $x = 125;
        $y = 5;
        $w = 80;
        $h = 30.1;
        $this->addImage($path, $x, $y, $w, $h, 0);
    }

    // Label and number of invoice/estimate
    function addCompanyAddress($address)
    {
        $w = 90;
        $h = 4.5;
        $x  = $this->w - $w;
        $y  = 38;

        $this->SetFont( "Arial", "", 10);
        $this->SetXY($x, $y);
        $this->MultiCell($w, $h, $address, 0, "C");
    }

    function LeftColumn($data = []) {
        $lineCount = count($data);
        $lineHeight = 5;
        $text = '';

        foreach($data as $str) {
            $text .= $str."\n";
        }

        $x = 6;
        $y = 78;
        $h = $lineHeight * $lineCount;
        $w = 100;

        $this->SetFont( "Arial", "", 10);
        $this->SetFillColor(230, 230, 230);
        $this->Rect($x, $y, $w, $h, 'F');
        $this->SetXY($x+2, $y+2);
        $this->MultiCell( $w, 4.5, $text, 0, '', FALSE);
    }

    function RightColumn($data = [], $values = []) {
        $lineCount = count($data);
        $lineHeight = 0;
        $text = '';
        $value = '';

        foreach($data as $str) {
            $text .= $str."\n";
        }

        foreach($values as $str) {
            $value .= $str."\n";
        }

        $x = 100;
        $y = 83;
        $h = $lineHeight * $lineCount;
        $w = 46.5;

        $this->SetFont( "Arial", "", 10);
        $this->SetFillColor(255, 255, 255);
        $this->Rect($x, $y, $w, $h, 'F');
        $this->SetXY($x+2, $y+2);
        $this->MultiCell( $w, 6, $text, 0, 'R', false);
        $this->SetXY($x+$w+2, $y+2);
        $this->SetFont( "Arial", "B", 10);
        $this->MultiCell( $w, 6, $value, 0, 'L', false);
    }

    function AddFooter($totals = [], $ref = 'Web') {

        // Green Box
        $h = 30;
        $w = 115;
        $x = 6;
        $y = $this->h - $h - 13;

        $this->SetXY($x+2, $y-13.5);
        $this->WriteTag(0,5,"<pxl>Order Placed By <redxl>".$this->invoice_full_name."</redxl></pxl>");

        $this->SetXY($x+2, $y-7.5);
        $this->WriteTag(0,5,"<pxl>on <redxl>".$this->invoice_date."</redxl> by <redxl>".$ref."</redxl></pxl>");

        $this->SetFillColor(206, 255, 183);
        $this->Rect($x, $y, $w, $h, 'F');
        $this->SetXY($x+2, $y+2);
        $this->SetFont( "Arial", "", 10);
        $this->WriteTag(0,5,"<pl>Please make Cheques payable to: <bl>".$this->company_name."</bl></pl>");
        $this->SetXY($x+2, $y+9);
        $this->WriteTag(0,5,"<pl><bl>BACS</bl> payments to <bl>Natwest Bank</bl> - Account Number: <bl>".$this->account_number."</bl></pl>");
        $this->SetXY($x+2, $y+13);
        $this->WriteTag(0,5,"<pl>Sort Code: <bl>".$this->account_sortcode."</bl> - VAT Reg. Number: <bl>".$this->vat_number."</bl></pl>");
        $this->SetXY($x+2, $y+19);
        $this->WriteTag(0,5,"<pl>If paying by <bl>BACS</bl> please <bl>email remittance advice</bl> to:</pl>");
        $this->SetXY($x+2, $y+23);
        $this->WriteTag(0,5,"<pl>".$this->email."</pl>");

        ////////////
        // TOTALS //
        ////////////
        $grand_total = $totals['total'];
        unset($totals['total']);
        $h = 50;
        $w = 40;
        $x = 120;
        $y = $this->h - $h - 3.8;
        $this->Line(6, $y-6, $this->w - 6, $y-6);

        $label_string = "Total Net Amount\nCarriage Net\nTotal VAT Amount";
        $this->SetFont( "Arial", "", 12);
        $this->SetXY($x+2, $y+2);
        $this->MultiCell($w, 7, $label_string, 0, 'R', false);

        $value_string = implode("\n", $totals);
        $this->SetFont( "Arial", "", 14);
        $this->SetXY($x+$w+2, $y+2);
        $this->MultiCell( $w, 7, $value_string, 0, 'R', false);

        $this->Line($x + 5, $y+25, $this->w - 6, $y+25);

        // Set new font for Grand Total
        $this->SetFont( "Arial", "B", 17);

        // Grand Total Label
        $this->SetXY($x+2, $y+28);
        $this->MultiCell($w, 9, "Grand Total", 0, 'R', false);

        // Grand Total Value
        $this->SetXY($x+$w+2, $y+28);
        $this->MultiCell($w, 9, $grand_total, 0, 'R', false);

        $this->Line($x + 5, $y+39.5, $this->w - 6, $y+39.5);

        $this->SetFont( "Arial", "", 8);
        $this->SetXY(6, $this->h-15);
        $this->SetLineHeight(4);
        $this->WriteTag(0,10, "<p>".$this->company_name." reserves the right to charge <red>Interest</red> on <red>Overdue Accounts</red> under The Late Payment of Commercial Debts (Interest) Act 1988</p>",0,"C");

        $this->SetXY(6, $this->h-11);
        $this->WriteTag(0,10, "<p>All goods supplied remain the property of Musicline Publications Ltd. until full payment has been received.</p>",0,"C");
    }

    function SetLineHeight($h) {
        $this->lineHeight = $h;
    }

    function addCols( $tab )
    {
        $i = 0;
        $r1  = 6;
        $r2  = $this->w - ($r1 * 2) ;
        $y1  = 120;
        $y2  = $this->h - 50 - $y1;
        $this->SetFontSize( 8);
        $this->SetFillColor(230, 230, 230);
        $this->SetXY( $r1, $y1 );
        $this->Rect( $r1, $y1, $r2, 9, "F");
        $colX = $r1;

        $this->columns = $tab;

        foreach($tab as $key => $value) {
            $offset = 2;
            if (!strstr($key, PHP_EOL)) {
                $offset = 3.4;
            }
            $this->SetXY($colX,$y1+$offset);
            if ($i++ == 0) {
                $align = 'L';
                $this->SetXY($colX,$y1+$offset);
            } else {
                $align = 'C';
            }
            $this->MultiCell( $value, 3, utf8_encode ($key), 0, $align);
            $colX += $value;
        }

    }

    function addLineFormat( $tab )
    {
        foreach($this->columns as $key => $value) {
            if ( isset( $tab["$key"] ) )
                $this->format[ $key ] = $tab["$key"];
        }
    }

    function addLine($line, $tab )
    {
        $ordonnee     = 6;
        $maxSize      = $line;

        reset( $this->columns );
        foreach($this->columns as $key => $value) {
            $longCell  = $value;
            $texte     = $tab[ $key ];
            $formText  = $this->format[ $key ];
            $this->SetFontSize( 7);
            $this->SetXY( $ordonnee, $line-1);
            $this->MultiCell( $longCell, 4 , $texte, 0, $formText);
            if ( $maxSize < ($this->y) )
                $maxSize = $this->y;
            $ordonnee += $value;
        }

        return ( $maxSize - $line );
    }

}
